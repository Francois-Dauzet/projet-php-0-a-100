<?php

declare(strict_types=1);

//////////////////// Variables ////////////////////

(int) $randomNumber = 0;
(int) $playerNumber = -1;
(int) $countGame = 0;
(string) $difficultiesChoice = "";

//////////////////// Difficulties ////////////////////

while (!$difficultiesChoice) {
    print ('0 à 100 = 1' . PHP_EOL . '0 à 1000 = 2' . PHP_EOL . '0 à 10 000 = 3') . PHP_EOL;
    $difficultiesChoice = trim(strval(fgets(STDIN)));
}

//////////////////// Game ////////////////////

if ("1" == $difficultiesChoice) {
    $randomNumber = random_int(0, 100);
    gameResult($randomNumber, $playerNumber, 100, $countGame);
} else if ("2" == $difficultiesChoice) {
    $randomNumber = random_int(0, 1000);
    gameResult($randomNumber, $playerNumber, 1000, $countGame);
} else {
    $randomNumber = random_int(0, 10000);
    gameResult($randomNumber, $playerNumber, 10000, $countGame);
}

//////////////////// function ////////////////////

function gameResult($randomNumber, $playerNumber, $maxNumber, $countGame)
{
    while ($randomNumber != $playerNumber) {
        print('Choisir un nombre :');
        $playerNumber = intval(trim(strval(fgets(STDIN))));
        if ($playerNumber >= 0 && $playerNumber <= $maxNumber) {
            if ($playerNumber > $randomNumber) {
                print('Plus bas' . PHP_EOL);
                $countGame++;
            } else if ($playerNumber < $randomNumber) {
                print('plus haut' . PHP_EOL);
                $countGame++;
            } else {
                printf(PHP_EOL . 'Vous avez gagné en %d parties', $countGame);
            }
        } else {
            printf('Choisir un nombre entre 0 et %d' . PHP_EOL, $maxNumber);
        }
    }
}
